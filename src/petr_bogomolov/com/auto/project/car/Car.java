package petr_bogomolov.com.auto.project.car;

import petr_bogomolov.com.auto.project.car.properties_of_car.BodyType;
import petr_bogomolov.com.auto.project.car.properties_of_car.Brakes;
import petr_bogomolov.com.auto.project.car.elements.Motor;
import petr_bogomolov.com.auto.project.car.elements.Transmission;
import petr_bogomolov.com.auto.project.car.elements.Wheels;

public class Car {

    Motor motor = new Motor();
    Transmission transmission = new Transmission();
    Wheels wheels = new Wheels();

    private String nameBrand = "Toyota";
    private String nameModel = "Camry";
    private BodyType bodyType = BodyType.SEDAN;
    private Motor motor1 = motor;
    private Transmission transmission1 = transmission;
    private Brakes brakes = Brakes.METAL;
    private Wheels wheels1 = wheels;
    private int maxSpeed = 250;
    private double accelerationFrom_0_before_100 = 7.6;
    private int weight = 1800;
    private int maxWeight = 2200;

    @Override
    public String toString() {
        return "Name Brand - " + nameBrand +
                " nameModel=" + nameModel +
                " bodyType=" + bodyType +
                " type of motor " + motor +
                " transmission=" + transmission +
                " brakes=" + brakes +
                " wheels=" + wheels +
                " maxSpeed=" + maxSpeed +
                " acceleration 0 - 100 km - " + accelerationFrom_0_before_100 +
                " weight=" + weight + " kg " +
                " maxWeight - " + maxWeight + " kg ";
    }
}
