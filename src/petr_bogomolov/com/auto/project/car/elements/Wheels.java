package petr_bogomolov.com.auto.project.car.elements;

import petr_bogomolov.com.auto.project.car.elements.properties_of_elements.ColorWheels;

public class Wheels {

    private ColorWheels colorWheels = ColorWheels.SILVER;

    @Override
    public String toString() {
        return "Wheels{" +
                "colorWheels=" + colorWheels +
                '}';
    }
}
