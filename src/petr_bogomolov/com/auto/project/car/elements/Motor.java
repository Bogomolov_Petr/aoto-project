package petr_bogomolov.com.auto.project.car.elements;

import petr_bogomolov.com.auto.project.car.elements.properties_of_elements.TypeOfMotor;

public class Motor {

    private TypeOfMotor typeOfMotor = TypeOfMotor.BENZIN;
    private int power = 220;
    private double volumMotor = 2.5;

    @Override
    public String toString() {
        return  typeOfMotor +
                ", power=" + power +
                ", volumMotor=" + volumMotor +
                '}';
    }
}
