package petr_bogomolov.com.auto.project.car.elements;

import petr_bogomolov.com.auto.project.car.elements.properties_of_elements.Drive;
import petr_bogomolov.com.auto.project.car.elements.properties_of_elements.TypeOfTransmission;

public class Transmission {

    private TypeOfTransmission typeOfTransmission = TypeOfTransmission.AUTO;
    private int numberOfStages = 7;
    private Drive drive = Drive.FRONT;

    @Override
    public String toString() {
        return "Transmission{" +
                "typeOfTransmission=" + typeOfTransmission +
                ", numberOfStages=" + numberOfStages +
                ", drive=" + drive +
                '}';
    }
}
